using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject modalPanel;
    public GameObject startGamePanel;
    public GameObject dialogBorder;
    public static GameManager instance;
    private const int freezeGame = 0;
    private const int unfreezeGame = 1;
    public AudioSource backgroundMusic; // Referência para o AudioSource da música de fundo

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        PlayBackgroundMusic(); // Chama a função para iniciar a música de fundo
    }

    // Função para iniciar a música de fundo
    void PlayBackgroundMusic()
    {
        if (backgroundMusic != null) // Verifica se existe um AudioSource
        {
            backgroundMusic.loop = true; // Define para que a música fique em loop
            backgroundMusic.Play(); // Inicia a reprodução da música
        }
        else
        {
            Debug.LogError("AudioSource de música de fundo não atribuído ao GameManager.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Restart()
    {
        if (modalPanel.activeInHierarchy)
        {
            modalPanel.SetActive(false);
        }
        if (startGamePanel.activeInHierarchy)
        {
            startGamePanel.SetActive(false);
            
            dialogBorder.SetActive(false);

        
            // Reset time scale
            Time.timeScale = unfreezeGame;
        }
    }
}

