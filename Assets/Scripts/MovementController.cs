using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public GameObject currentNode;
    public float speed = 4f;
    public string direction = "";
    public string lastMovingDirection = "";
    private bool isMoving = false;
    public AudioSource moveSound;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            NodeController currentNodeController = currentNode.GetComponent<NodeController>();
            transform.position = Vector2.MoveTowards(transform.position, currentNode.transform.position, speed * Time.deltaTime);
            float distance = Vector2.Distance(transform.position, currentNode.transform.position);
            if (distance < 0.001f) // Ajuste a tolerância conforme necessário
            {
                transform.position = currentNode.transform.position;
                isMoving = false;

                GameObject newNode = currentNodeController.GetNodeFromDirection(direction);
                if (newNode != null)
                {
                    currentNode = newNode;
                    lastMovingDirection = direction;    
                }
                else 
                {
                    direction = lastMovingDirection;
                    newNode = currentNodeController.GetNodeFromDirection(direction);
                    if (newNode != null)
                    {
                        currentNode = newNode;
                    }
                }
            }
        }
    }

    public void SetDirection(string newDirection)   
    {
        direction = newDirection;
        isMoving = true;

        // Reproduz o som de movimento
        if (moveSound != null)
        {
            moveSound.Play();
            Debug.Log("Som de movimento reproduzido.");
        }
    }


}

